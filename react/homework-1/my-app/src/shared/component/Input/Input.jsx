import React from "react";
import './Input.css';

const Input = (props) => {
    const {
        type = 'text', name, placeholder, className, id,
        labelFor, labelText, value, onChange
        } = props;
    let classPrefix = "";
    type === 'submit' ? classPrefix = 'btn' : classPrefix = 'form-control';
    const fullClassName = `${classPrefix} ${className || ""}`;
    return (
        <div className="form-group">
            <label htmlFor={labelFor}>{labelText}</label>
            <input type={type} name={name} placeholder={placeholder}
                   className={fullClassName} id={id} value={value} onChange={(e) => onChange(e)}/>
        </div>
    )
}
export default Input;