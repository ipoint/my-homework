import React, {Component} from "react";
import './BookEdit.css';
import {fields} from "../BookForm/fields";
import Input from "../../../../shared/component/Input";

class BookEdit extends Component {
    state = {
        title: this.props.title,
        author: this.props.author,
        isbn: this.props.isbn,
        edit: true
    }

    handleChange = ({target}) => {
        this.setState({
            [target.name]: target.value
        })
    }

    render() {
        const {title, author, isbn} = fields;
        const {title: titleValue, author: authorValue, isbn: isbnValue} = this.state;
        const {handleChange} = this
        return (
            <tr>
                <td>
                    <Input {...title} onChange={handleChange} value={titleValue}/>
                </td>
                <td>
                    <Input {...author} onChange={handleChange} value={authorValue}/>
                </td>
                <td>
                    <Input {...isbn} onChange={handleChange} value={isbnValue}/>
                </td>
                <td>
                    <input type="submit" value="Update" className="btn btn-primary"
                           onClick={() => this.props.handleUpdate(titleValue, authorValue, isbnValue)}/></td>
            </tr>
        )
    }
}

export default BookEdit;