import React, {Component} from "react";
import './BookList.css';

import BookListItem from "../BookListItem";

class BookList extends Component {
    render() {
        const {bookData, handleDeleteBook, handleEditBook} = this.props
        const countBook = bookData.length
        const bookItem = bookData.map((item, index) =>
            <BookListItem {...item}
                          handleEditBook={(title, author, isbn) => handleEditBook(index, title, author, isbn)}
                          handleDelete={() => handleDeleteBook(item)}/>)
        return (
            <>
                <h3 id="book-count" className="book-count mt-5">Всего книг: {countBook}</h3>
                <table className="table table-striped mt-2">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>ISBN#</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="book-list">
                        {bookItem}
                    </tbody>
                </table>
            </>
        )
    }
}

export default BookList;