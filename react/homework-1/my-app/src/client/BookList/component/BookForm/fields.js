export const fields = {
    title: {
        type: "text",
        name: "title",
        labelFor: "title",
        labelText: "Title"
    },
    author: {
        type: "text",
        name: "author",
        labelFor: "author",
        labelText: "Author"
    },
    isbn: {
        type: "text",
        name: "isbn",
        labelFor: "isbn",
        labelText: "ISBN#"
    },
    submit: {
        type: "submit",
        className: "btn-primary",
        value: "Add book"
    }
}
