import React, {Component} from "react";
import './BookForm.css';

import Input from "../../../../shared/component/Input";
import BookList from "../BookList";

import {fields} from './fields.js';

class BookForm extends Component {
    state = {
        title: '',
        author: '',
        isbn: '',
        bookData: [],
        errorMessage: ''
    }

    handleEditBook = (index, title, author, isbn) => {
        this.setState(({bookData})=>{
            const newBookData = [...bookData]
            newBookData[index] = {title, author, isbn}
            return {
                bookData: newBookData
            }
        })
    }

    handleDeleteBook = (book) => {
        const {bookData} = this.state;
        const idx = bookData.findIndex(({title}) => title === book.title);
            this.setState(({bookData}) => {
                const newBookList = [...bookData.slice(0, idx), ...bookData.slice(idx+1)];
                return {
                    bookData: newBookList
                }
            });
    };

    handleChange = ({target}) => {
        this.setState({
            [target.name]: target.value
        });
        if (target.value) {
            this.setState({
                errorMessage: ''
            })
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {title, author, isbn} = this.state;
        const book = {title, author, isbn}

        if (title && author && isbn) {
            this.setState({
                bookData: [...this.state.bookData, book]
            })
            this.setState({
                title: '',
                author: '',
                isbn: '',
                errorMessage: ''
            })
        }
        else {
            this.setState({
                errorMessage: 'Please, feel all fields'
            })
        }
    }

    render() {
        const {title, author, isbn, submit} = fields;
        const {title: titleValue, author: authorValue, isbn: isbnValue, bookData, errorMessage} = this.state;
        const {handleChange, handleSubmit, handleDeleteBook, handleEditBook} = this
        return (
            <>
                <div className="row">
                    <div className="col-lg-4">
                        <form id="add-book-form" onSubmit={handleSubmit}>
                            <Input {...title} value={titleValue} onChange={handleChange}/>
                            <Input {...author} value={authorValue} onChange={handleChange}/>
                            <Input {...isbn} value={isbnValue} onChange={handleChange}/>
                            <Input {...submit}/>
                        </form>
                        <span className='text-danger'>{errorMessage}</span>
                    </div>
                </div>
                <BookList bookData={bookData} handleDeleteBook={handleDeleteBook} handleEditBook={handleEditBook}/>
            </>
        )
    }
}

export default BookForm;