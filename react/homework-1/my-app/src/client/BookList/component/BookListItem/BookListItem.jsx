import React, {Component} from "react";
import './BookListItem.css';

import BookEdit from "../BookEdit";

class BookListItem extends Component {
    state = {
        title: '',
        author: '',
        isbn: '',
        edit: false
    }
    handleEdit = () => {
        const {edit} = this.state
        const {title, author, isbn} = this.props
        this.setState({
            title,
            author,
            isbn,
            edit: !edit
        })

    }
    handleUpdate = (title, author, isbn) => {
        const {edit} = this.state
        this.props.handleEditBook(
            title,
            author,
            isbn
        )
        this.setState({
            edit: !edit
        })
    }

    render() {
        const {title, author, isbn, handleDelete} = this.props
        const {edit} = this.state
        const {handleEdit, handleUpdate} = this
        return (
            <>
                {!edit &&
                <tr data-id="12">
                    <td>{title}</td>
                    <td>{author}</td>
                    <td>{isbn}</td>
                    <td>
                        <a href="#" className="btn btn-info btn-sm" onClick={handleEdit}>
                            <i className="fas fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a href="#" className="btn btn-danger btn-sm btn-delete" onClick={handleDelete}>
                            <i className="far fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
                }
                {edit &&
                <BookEdit {...this.props} handleUpdate={handleUpdate}/>
                }
            </>
        )
    }
}

export default BookListItem;