import React from 'react';
import './bootstrap.min.css'
import './App.css';
import BookForm from "./client/BookList/component/BookForm";


function App() {
    return (
        <div className="container mt-4">
            <h1 className="display-4 text-center">
                <i className="fas fa-book-open text-primary"></i>
                <span className="text-secondary">Book</span>
                List
            </h1>
            <p className="text-center">Add your book information to store it in database.</p>
            <BookForm/>
        </div>
    )
}

export default App;
