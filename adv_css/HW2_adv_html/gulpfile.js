const gulp = require('gulp');
const del = require('del');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const concat =  require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const prefix = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const jsminify = require('gulp-js-minify');
const jsuglyfy = require('gulp-uglify');


const DEL = () =>
    del(['./dist']);

const CSS = () =>
    gulp.src('./src/scss/*.scss')
        .pipe(sass())
        .pipe(concat('style.min.css'))
        .pipe(prefix({cascade:false}))
        .pipe(cleanCSS({level:2}))
        .pipe(gulp.dest('./dist/css/'));

const JS = () =>
    gulp.src('./src/js/**/*.js')
        .pipe(concat('script.min.js'))
        .pipe(jsminify())
        .pipe(jsuglyfy())
        .pipe(gulp.dest('./dist/js/'));

const HTML = () =>
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./dist/'));

const IMG = () =>
    gulp.src('./src/img/**/*.{jpg,png,svg,gif,webp}')
        .pipe(imagemin({optimizationLevel: 5}))
        .pipe(gulp.dest('./dist/img/'));

const watch = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('./src/scss/**/*.scss', CSS);
    gulp.watch('./src/index.html', HTML);
    gulp.watch('./src/js/**/*.js', JS);
    gulp.watch('./src/img/**/*.{jpg,png,svg,gif,webp}', IMG);
    gulp.watch('./src/**/*.*').on('change', browserSync.reload);
}

gulp.task('build', gulp.series(DEL, CSS, HTML, JS, IMG));
gulp.task('dev', gulp.series("build", watch));