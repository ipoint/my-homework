
const filmsURL = 'https://swapi.dev/api/films/'
const req = new XMLHttpRequest;
const filmsList = document.getElementById('films-list')
req.open('GET', filmsURL);
req.send();
req.responseType = 'json';
req.onload = () => {
    if (req.status >= 300) {
        console.log(`Error ${req.status}:${req.statusText}`)
    } else {
        const res = req.response;
        res.results.map(item => {
            const film = document.createElement('div')
            film.id = 'film-card';
            film.classList = 'film-item';
            film.insertAdjacentHTML('beforeend',
                `<p class="film-id">${item.episode_id}</p>
                 <h2 class="film-title">${item.title}</h2>
                 <p class="film-crawl">${item.opening_crawl}</p>`)
            filmsList.append(film)
            const personList = document.createElement('ul');
            personList.classList = 'person-list';
            item.characters.map((person) => {
                const newReq = new XMLHttpRequest;
                newReq.open('GET', person);
                newReq.send();
                newReq.responseType = 'json';
                newReq.onload = () => {
                    if (newReq.status >= 300) {
                        console.log(`Error ${newReq.status}:${newReq.statusText}`)
                    }
                    else {
                       const personName = `<li class="person-name">${newReq.response.name}</li>`;
                       personList.insertAdjacentHTML('beforeend', personName)
                       film.append(personList)
                    }
                }
            })
        })
    }
}