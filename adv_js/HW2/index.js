const SIZE_SMALL = {
    price: 50,
    calories: 20,
};
const SIZE_LARGE = {
    price: 100,
    calories: 40,
};
const STUFFING_CHEESE = {
    price: 10,
    calories: 20,
};
const STUFFING_SALAD = {
    price: 20,
    calories: 5,
};
const STUFFING_POTATO = {
    price: 15,
    calories: 10,
};
const TOPPING_MAYO = {
    price: 20,
    calories: 5,
};
const TOPPING_SPICE = {
    price: 15,
    calories: 0,
};


class Hamburger {
    constructor(size, stuffing) {
        const sizeName = [SIZE_SMALL, SIZE_LARGE];
        try {
            // проверка названия размера
            if (!sizeName.includes(size)) {
                throw new HamburgerException("no size given")
            }
            const hamStuffing = [STUFFING_CHEESE, STUFFING_SALAD, STUFFING_POTATO];
            // проверка названия начинка
            if (!hamStuffing.includes(stuffing)) {
                throw new HamburgerException("no stuffing given")
            }
            this.size = size;
            this.stuffing = stuffing;
            this.topping = [];
        }
        catch (err) {
            console.error(`${err.name}:${err.message}`);
        }
    }


    addTopping(topping) {
        const hamTopping = [TOPPING_MAYO, TOPPING_SPICE];
        try {
            if(!hamTopping.includes(topping)) {
                throw new HamburgerException("no topping given");
            }
            if(this.topping.includes(topping)) {
                throw new HamburgerException("such topping are already exists");
            }
            this.topping.push(topping);
        }
        catch(err){
            console.error(`${err.name}:${err.message}`);
        }
    }

    removeTopping(topping) {
        return (this.topping = this.topping.filter(item => item == topping))
        //      var index = this.topping.indexOf(topping);
        //   if (index !== -1) {
        //     this.topping.splice(index, 1);
        //   }
    }
    getToppings() {
        return this.topping;
    }
    getSize() {
        return this.size;
    }
    getStuffing() {
        return this.stuffing;
    }
    calculatePrice() {
        let price = this.size.price + this.stuffing.price;
        for(let topping of this.topping) {
            price += topping.price;
        }
        return price;
    }
    calculateCalories() {
        let calories = this.size.calories + this.stuffing.calories;
        for(let topping of this.topping) {
            calories += topping.calories;
        }
        return calories;
    }

}

class HamburgerException {
    constructor(message) {
    this.message = message;
    }
}

// маленький гамбургер с начинкой из сыра
const hamburger = new Hamburger(SIZE_LARGE, STUFFING_POTATO);
console.log(hamburger)
// добавка из майонеза
hamburger.addTopping(TOPPING_SPICE);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
//я тут передумал и решил добавить еще приправу
hamburger.addTopping(TOPPING_MAYO);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === SIZE_LARGE); // -> false
// Убрать добавку
console.log("Have %d toppings", hamburger.getToppings().length); // 1
console.log(hamburger)