function Hamburger(size, stuffing) {
    var sizeName = [Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE];
    try {
        // проверка названия размера
        if (!sizeName.includes(size)) {
            throw new HamburgerException("no size given")
        }
        var hamStuffing = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD, Hamburger.STUFFING_POTATO];
        // проверка названия начинка
        if (!hamStuffing.includes(stuffing)) {
            throw new HamburgerException("no stuffing given")
        }
        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
    }
    catch (err) {
        console.error(`${err.name}:${err.message}`);
    }
}

Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20,
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40,
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20,
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5,
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10,
};
Hamburger.TOPPING_MAYO = {
    price: 20,
    calories: 5,
};
Hamburger.TOPPING_SPICE = {
    price: 15,
    calories: 0,
};


Hamburger.prototype.addTopping = function (topping) {
    var hamTopping = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE];
    try {
        if(!hamTopping.includes(topping)) {
            throw new HamburgerException("no topping given");
        }
        if(this.topping.includes(topping)) {
            throw new HamburgerException("such topping are already exists");
        }
        this.topping.push(topping);
    }
    catch(err){
        console.error(`${err.name}:${err.message}`);
    }
}

Hamburger.prototype.removeTopping = function (topping) {
    return (this.topping = this.topping.filter(item => item == topping))
    //      var index = this.topping.indexOf(topping);
    //   if (index !== -1) {
    //     this.topping.splice(index, 1);
    //   }
}
Hamburger.prototype.getToppings = function () {
    return this.topping;
}
Hamburger.prototype.getSize = function () {
    return this.size;
}
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
}
Hamburger.prototype.calculatePrice = function () {
    var price = this.size.price + this.stuffing.price;
    for(var topping of this.topping) {
        price += topping.price;
    }
    return price;
}
Hamburger.prototype.calculateCalories = function () {
    var calories = this.size.calories + this.stuffing.calories;
    for(var topping of this.topping) {
        calories += topping.calories;
    }
    return calories;
}
function HamburgerException(message) {
    this.message = message;
}

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
console.log(hamburger)
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
//я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
console.log("Have %d toppings", hamburger.getToppings().length); // 1
console.log(hamburger)